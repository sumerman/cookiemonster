require 'rubygems'
require 'sinatra'
require 'net/http'
require 'net/https'
require 'uri'
require 'json'

$LOAD_PATH.unshift File.dirname(__FILE__)
require 'models.rb'

PREFIX = '/dl'

use Rack::Session::Cookie, :key	         => 'rack.session',
                           :expire_after => 864000, # In seconds
                           :secret       => 'cookiemonster'

helpers do
	def valid_uri(url)
		uri = URI.parse(url)
		http = Net::HTTP.new(uri.host, uri.port) 
		http.use_ssl = (uri.scheme == 'https')
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		res = http.head(uri.path) 
		raise halt 400 unless (res.is_a? Net::HTTPSuccess) || (res.is_a? Net::HTTPRedirection)
		uri
	end

	def new_tracking_id
		(rand * 10000).floor.to_s + '@' + Time.now.to_s
	end

	def group_views_by(k1, k2)
		res = {}
		ViewRecord.all.each do |vr| 
			res[vr.send(k1).id] ||= {}
			res[vr.send(k1).id][vr.send(k2).id] = vr.count
		end
		res
	end
end

get '/traces' do
	content_type 'application/json'
	JSON.pretty_generate group_views_by(:cookie, :image)
end

get '/views_count' do
	content_type 'application/json'
	JSON.pretty_generate group_views_by(:image, :cookie)
end

get '/log' do
	content_type 'application/json'
	JSON.pretty_generate LogEntry.all
end

get PREFIX do
	content_type 'application/json'
	JSON.pretty_generate Image.all.map { |e| request.base_url + request.path + '/' + e.id }
end

get PREFIX+'/:id' do |id|
	img   = Image.get(id) or halt 404
	ip    = env['HTTP_X_REAL_IP'] ||= env['REMOTE_ADDR']
	info  = request.env.reject{|k,v| ! k.start_with? 'HTTP_'} 
	info[:ip] = ip
	tr_id = session[:tracking_id] || new_tracking_id

	c = Cookie.first_or_create(:id => tr_id)
	l = LogEntry.first_or_create(:req_info => info)
	r = ViewRecord.first_or_create(:cookie => c, :image => img)
	r.count += 1
	r.log_entries << l
	r.save

	session[:tracking_id] = tr_id
	redirect Image.get(id).origin_url.to_s
end

put PREFIX+'/:id?' do |id|
	request.body.rewind
	uri = valid_uri(request.body.read)

	id ||= uri.filename
	img = Image.first_or_create({:id => id})
	img.origin_url = uri
	img.save!

	status 201
end

delete PREFIX+'/:id' do |id|
	img = Image.get(id)
	halt 500 unless ViewRecord.all(:image => img).destroy
	halt 500 unless img.destroy
	status 204
end

#get '/cleanup' do
	#LogEntry.all.destroy
	#ViewRecord.all.destroy
	#Cookie.all.destroy
#end
