require 'rubygems'
require 'data_mapper'

DataMapper.setup(:default, ENV['DATABASE_URL'] || "sqlite::memory:")

module URI
	def filename
		self.path.split('/').last
	end
end

class Image
	include DataMapper::Resource

	property :id,         String, :length => 1..100, :key => true
	property :origin_url, URI,    :required => true

	has n, :view_records
	has n, :cookies, 'Cookie', :through => :view_records, :via => :cookie
end

class Cookie
	include DataMapper::Resource

	property :id, String, :length => 1..100, :key => true

	has n, :view_records
	has n, :images, :through => :view_records
end

class LogEntry
	include DataMapper::Resource

	property :id,       Serial
	property :req_info, Json, :default => ""

	belongs_to :view_record
end

class ViewRecord
	include DataMapper::Resource

	property :id,    Serial
	property :count, Integer, :default => 0

	belongs_to :cookie, :key => true
	belongs_to :image,  :key => true
	has n,     :log_entries
end

DataMapper.finalize
DataMapper.auto_upgrade!
